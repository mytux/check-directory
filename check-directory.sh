#!/bin/sh
# Copyright 2014 Cyril Brulebois <cyril@mraw.org> for MyTux.
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

set -e

### BEGIN: configuration
#
#
# Directory to check:
DIR=/var/log
# Where to remember previously sent messages:
STAMP_DIR=/var/tmp/mytux-check-var-log
# Thresholds:
WARNING=40
FATAL=20
# How many files to mention:
MAX_FILES=20
# Who to warn:
TO="support@mytux.fr"
#
#
### END: configuration


# Take free space % from df, excluding the header line:
percent_taken=$(df -k "$DIR" | tail -n 1 | awk '{print $5}' | tr -d '%')
percent_free=$((100-$percent_taken))
# Fetch the mountpoint, just in case:
mountpoint=$(df -k "$DIR" | tail -n 1 | awk '{print $6}')

# Factorize checking and sending:
check_disc() {
  label="$1"
  limit="$2"

  if [ "$percent_free" -le "$limit" ]; then
    # We already sent a mail for this status, avoid doing so again:
    if [ -e "$STAMP_DIR/$label" ]; then
       echo "Already warned, exiting"
      exit 0
    fi

    # Otherwise, send an alert, remember, and exit:
    hostname=$(hostname -f)
    subject="[$hostname/MyTux] $label : disk space on $mountpoint"
    cat <<EOF | mail -s "$subject" "$TO"
The partition $mountpoint containing $DIR reached the level $label.
Only $percent_free % of free space (<= $limit %).

For your information the biggest files in $DIR are
the following ones:

$(ls -lSdh $DIR/* | head -n $MAX_FILES | awk '{ printf("% 8s %s\n", $5, $9) }')

To investigate:
  ssh root@$hostname


Do not forget to remove this file to reset the alert trigger once
the investigation is over:

  $STAMP_DIR/$label


Script : $0
EOF

    touch $STAMP_DIR/$label
    exit 0
  fi
}

# Check for fatal then warning threshold:
check_disc FATAL $FATAL
check_disc WARNING $WARNING
